package org.tetris.screens;

import org.tetris.GameButton;
import org.tetris.UberTetris;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class OptionScreen extends BaseScreen {

    private GameButton closeBtn;

    public OptionScreen(UberTetris parent) {
        super(parent);

        closeBtn = new GameButton(10, 100, "res/btn_opt_idle.png", "res/btn_opt_pressed.png");

        addMouseListener(new MAdapter());
        start();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        closeBtn.draw(g);
    }

    private class MAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            closeBtn.setPressed(closeBtn.inRegion(e.getX(), e.getY()));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (closeBtn.inRegion(e.getX(), e.getY()) && closeBtn.isPressed()) {
                stop();
                parent.changeScreen(new MenuScreen(parent));
            }
            closeBtn.setPressed(false);
        }
    }
}
