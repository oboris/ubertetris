package org.tetris.screens;

import org.tetris.UberTetris;

import javax.swing.*;

public class BaseScreen extends JComponent implements Runnable {
    private boolean isWorking;
    private final long SLEEP_TIME = 40;
    public UberTetris parent;

    public BaseScreen(UberTetris parent) {
        this.parent = parent;
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public void start(){
        isWorking = true;
        new Thread(this).start();
    }

    public void stop(){
        isWorking = false;
    }

    @Override
    public void run() {
        while (isWorking){
            update(SLEEP_TIME);
            repaint();
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(long tick){

    }
}
