package org.tetris.screens;

import org.tetris.GameButton;
import org.tetris.UberTetris;
import org.tetris.game.GameField;

import java.awt.*;
import java.awt.event.*;

public class GameScreen extends BaseScreen {

    private GameButton closeBtn;

    private GameField gameField;

    public GameScreen(UberTetris parent) {
        super(parent);
        closeBtn = new GameButton(450, 400, 50, 30, Color.BLUE, "Exit", null);

        gameField = new GameField();

        addKeyListener(new KAdapter());
        addMouseListener(new MAdapter());

        start();
    }

    @Override
    public void update(long tick) {
        requestFocus();
        gameField.update(tick);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        gameField.draw(g);
        closeBtn.draw(g);
    }

    private class MAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            closeBtn.setPressed(closeBtn.inRegion(e.getX(), e.getY()));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (closeBtn.inRegion(e.getX(), e.getY())) {
                stop();
                parent.changeScreen(new MenuScreen(parent));
            }
            closeBtn.setPressed(false);
        }
    }

    private class KAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyCode != 'p' && keyCode != 'P') {
                if (!gameField.isPause()) {
                    switch (keyCode) {
                        case KeyEvent.VK_LEFT:
                            gameField.tryMoveSide(-1);
                            break;
                        case KeyEvent.VK_RIGHT:
                            gameField.tryMoveSide(1);
                            break;
                        case KeyEvent.VK_DOWN:
                            gameField.tryRotate(-1);
                            break;
                        case KeyEvent.VK_UP:
                            gameField.tryRotate(1);
                            break;
                        case KeyEvent.VK_SPACE:

                            break;
                    }
                }
            } else {
                gameField.setPause(!gameField.isPause());
            }
        }
    }
}
