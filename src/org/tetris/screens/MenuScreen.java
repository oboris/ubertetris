package org.tetris.screens;

import org.tetris.GameButton;
import org.tetris.PropClass;
import org.tetris.Sound;
import org.tetris.UberTetris;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MenuScreen extends BaseScreen {

    private GameButton optionsBtn, startBtn, closeBtn;

    public MenuScreen(UberTetris parent) {
        super(parent);
        optionsBtn = new GameButton(10, 10, "res/btn_opt_idle.png", "res/btn_opt_pressed.png");
        startBtn = new GameButton(10, optionsBtn.getTop() + optionsBtn.getHeight() + 10, "res/btn_play_idle.png", "res/btn_play_pressed.png");
        closeBtn = new GameButton(10, startBtn.getTop() + startBtn.getHeight() + 10, startBtn.getWidth(), startBtn.getHeight() * 2 / 3, Color.BLUE, "Exit", null);

        addMouseListener(new MAdapter());

        if (PropClass.bgSound == null) {
            PropClass.bgSound = Sound.playSoundLoop("res/bg.mid");
        }

        start();

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        optionsBtn.draw(g);
        startBtn.draw(g);
        closeBtn.draw(g);
    }


    private class MAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            if (optionsBtn.inRegion(e.getX(), e.getY())) {
                optionsBtn.setPressed(true);
            } else if (startBtn.inRegion(e.getX(), e.getY())) {
                startBtn.setPressed(true);
            } else if (closeBtn.inRegion(e.getX(), e.getY())) {
                closeBtn.setPressed(true);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (optionsBtn.inRegion(e.getX(), e.getY()) && optionsBtn.isPressed()) {
                stop();
                parent.changeScreen(new OptionScreen(parent));
            } else if (startBtn.inRegion(e.getX(), e.getY()) && startBtn.isPressed()) {
                stop();
                parent.changeScreen(new GameScreen(parent));
            } else if (closeBtn.inRegion(e.getX(), e.getY()) && closeBtn.isPressed()) {
                stop();
                System.exit(0);
            }

            optionsBtn.setPressed(false);
            startBtn.setPressed(false);
            closeBtn.setPressed(false);
        }
    }
}
