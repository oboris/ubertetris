package org.tetris;

public interface BrickOwner {
    int getOwnerX();

    int getOwnerY();
}
