package org.tetris;

import org.tetris.screens.*;
import javax.swing.*;

public class UberTetris extends JFrame {

    private UberTetris() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(700, 700);
        getContentPane().add(new MenuScreen(this));
        setResizable(false);
        setVisible(true);
    }

    public void changeScreen(BaseScreen screen){
        getContentPane().removeAll();
        getContentPane().invalidate();

        getContentPane().add(screen);
        getContentPane().revalidate();
    }

    public static void main(String[] args) {
        new UberTetris();
    }
}
