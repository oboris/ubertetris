package org.tetris;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameButton {
    private BufferedImage idleBg, pressedBg;
    private boolean isPressed = false;
    private int left, top, height, width;
    private Color color, textColor = Color.WHITE;
    private String text;

    public GameButton(int left, int top, String idleStr, String pressedStr) {
        this.left = left;
        this.top = top;

        try {
            idleBg = ImageIO.read(new File(idleStr));
            pressedBg = ImageIO.read(new File(pressedStr));
            height = idleBg.getHeight();
            width = idleBg.getWidth();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GameButton(int left, int top, int width, int height, Color color, String text, Color textColor) {
        this.left = left;
        this.top = top;
        this.height = height;
        this.width = width;
        this.color = color;
        this.text = text;
        if (null != textColor) {
            this.textColor = textColor;
        }
    }

    public void draw(Graphics g){
        if (isPressed) {
            if (null != pressedBg) {
                g.drawImage(pressedBg, left, top, null);
            } else {
                drawButtonRect(g, color.darker());
            }
        } else {
            if (null != pressedBg) {
                g.drawImage(idleBg, left, top, null);
            } else {
                drawButtonRect(g, color);
            }
        }
    }

    private void drawButtonRect(Graphics g, Color color){
        g.setColor(color);

        g.fillRect(left, top, width, height);
        if (null != text) {
            g.setColor(textColor);
            Font font = new Font("Serif", Font.BOLD, 16);
            FontMetrics metrics = g.getFontMetrics(font);
            int x = left + (width - metrics.stringWidth(text)) / 2;
            int y = top + ((height - metrics.getHeight()) / 2) + metrics.getAscent();
            g.setFont(font);
            g.drawString(text, x, y);
        }
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public int getTop() {
        return top;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public boolean inRegion(int x, int y){
        return x >= left && x <= left + width && y >= top && y <= top + height;
    }
}
