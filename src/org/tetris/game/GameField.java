package org.tetris.game;

import org.tetris.BrickOwner;
import org.tetris.Sound;

import java.awt.*;

public class GameField implements BrickOwner {
    public static final int GLASS_WIDTH = 10;
    public static final int GLASS_HEIGHT = 20;
    public static final int MARGIN = 10;

    private Brick[][] glass = new Brick[GLASS_HEIGHT][GLASS_WIDTH];
    public static final int CELL_SIZE = 30;

    public Block currentBlock, nextBlock;
    private float blockVelocity = 5.f;
    private boolean pause = false;

    public GameField() {
        for (int i = 0; i < glass.length; i++) {
            for (int j = 0; j < glass[i].length; j++) {
                glass[i][j] = null;
            }
        }
        currentBlock = new Block();
        currentBlock.setVelocity(blockVelocity);
        nextBlock = new Block();
        nextBlock.setX(11);
        nextBlock.setVelocity(0f);
    }

    private void drawGlass(Graphics g) {
        int dy = 0;
        for (Brick[] row : glass) {
            int dx = 0;
            for (Brick brick : row) {
                if (brick == null) {
                    g.drawRect(MARGIN + dx, MARGIN + dy, CELL_SIZE, CELL_SIZE);
                } else
                    brick.draw(g);
                dx += CELL_SIZE;
            }
            dy += CELL_SIZE;
        }
    }

    private long timeOf;

    public void update(long tick) {
        if (!pause) {
            timeOf += tick;
            if (timeOf / 1000.0f * currentBlock.getVelocity() >= 1.0f) {
                tryMoveDown();
                timeOf = 0;
            }
        }
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public boolean isPause() {
        return pause;
    }

    private boolean checkCanMoveDown() {
        boolean canMove = true;
        if (currentBlock.getY() + currentBlock.getBlockMask().length < GLASS_HEIGHT) {
            for (int i = 0; i < currentBlock.getBlockMask().length; i++) {
                for (int j = 0; j < currentBlock.getBlockMask()[0].length; j++) {
                    if (currentBlock.getBlockMask()[i][j] == 1 &&
                            glass[currentBlock.getY() + i + 1][currentBlock.getX() + j] != null) {
                        canMove = false;
                        break;
                    }
                }
            }
        } else {
            canMove = false;
        }
        return canMove;
    }

    private boolean checkCanMoveSide(int direction) {
        boolean canMove = true;
        if ((direction == -1 && currentBlock.getX() > 0) || (direction == 1 && currentBlock.getX() + currentBlock.getBlockMask()[0].length < GLASS_WIDTH)) {
            for (int i = 0; i < currentBlock.getBlockMask().length; i++) {
                for (int j = 0; j < currentBlock.getBlockMask()[0].length; j++) {
                    if (currentBlock.getBlockMask()[i][j] == 1 &&
                            glass[currentBlock.getY() + i][currentBlock.getX() + j + direction] != null) {
                        canMove = false;
                        break;
                    }
                }
            }
        } else {
            canMove = false;
        }
        return canMove;
    }

    private void nextStep() {
        for (Brick brick : currentBlock.getBricks()) {
            /*brick.setOwnerX(0);
            brick.setOwnerY(0);*/
            brick.setOwner(this);
            brick.setY(brick.getY() + currentBlock.getY());
            brick.setX(brick.getX() + currentBlock.getX());
            glass[brick.getY()][brick.getX()] = brick;
        }

        tryDeleteLines();

        currentBlock = nextBlock;
        currentBlock.setX((GameField.GLASS_WIDTH - currentBlock.getBlockMask()[0].length) / 2);
        currentBlock.setY(0);
        currentBlock.setVelocity(blockVelocity);
        nextBlock = new Block();
        nextBlock.setX(11);
        nextBlock.setVelocity(0f);
    }

    private void tryDeleteLines() {
        for (int i = 0; i < glass.length; i++) {
            boolean isFullLine = true;
            for (int j = 0; j < glass[i].length; j++) {
                if (glass[i][j] == null) {
                    isFullLine = false;
                    break;
                }
            }
            if (isFullLine) {
                delGlassLine(i);
            }
        }
    }

    private void delGlassLine(int lineNumber) {
        for (int i = lineNumber; i > 0; i--) {
            for (int j = 0; j < glass[i].length; j++) {
                glass[i][j] = glass[i - 1][j];
                if (glass[i][j] != null) {
                    glass[i][j].setY(glass[i][j].getY() + 1);
                }
            }
        }

        for (int i = 0; i < glass[0].length; i++) {
            glass[0][i] = null;
        }
    }

    private void tryMoveDown() {
        if (checkCanMoveDown()) {
            currentBlock.moveDown();
        } else {
            if (!isGameOver()) {
                Sound.playSound("res/1.wav");
                nextStep();
            }
        }
    }

    private boolean isGameOver() {
        return currentBlock.getY() == 0;
    }

    public void tryMoveSide(int direction) {
        if (checkCanMoveSide(direction)) {
            currentBlock.moveSide(direction);
        }
    }

    private boolean checkCanRotate(int direction) {
        boolean canRotate = true;

        return canRotate;
    }

    public void tryRotate(int direction) {
        //if (checkCanRotate(direction)) {
            currentBlock.rotate(direction);
        if(!isGoodRotate()){
            currentBlock.rotate(-direction);
        }
            //}
    }

    private boolean isGoodRotate() {
        boolean goodRotate = true;
        if (currentBlock.getBlockMask()[0].length + currentBlock.getX() < GLASS_WIDTH &&
                currentBlock.getBlockMask().length + currentBlock.getY() < GLASS_HEIGHT){
            for (int i = 0; i < currentBlock.getBlockMask().length; i++) {
                for (int j = 0; j < currentBlock.getBlockMask()[0].length; j++) {
                    if (currentBlock.getBlockMask()[i][j] == 1 &&
                            glass[currentBlock.getY() + i][currentBlock.getX() + j] != null) {
                        goodRotate = false;
                        break;
                    }
                }
            }
        } else {
            goodRotate = false;
        }
        return goodRotate;
    }

    public void draw(Graphics g) {
        drawGlass(g);
        currentBlock.draw(g);
        nextBlock.draw(g);
    }

    @Override
    public int getOwnerX() {
        return 0;
    }

    @Override
    public int getOwnerY() {
        return 0;
    }
}
