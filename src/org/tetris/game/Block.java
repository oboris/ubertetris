package org.tetris.game;

import org.tetris.BrickOwner;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Block implements BrickOwner {

    private final int[][] SQUARE_BLOCK_MASK = {{1, 1}, {1, 1}};
    private final int[][] LINE_BLOCK_MASK = {{1, 1, 1, 1}};
    private final int[][] LEFT_L_BLOCK_MASK = {{1, 0}, {1, 0}, {1, 1}};
    private final int[][] RIGHT_L_BLOCK_MASK = {{0, 1}, {0, 1}, {1, 1}};
    private final int[][] LEFT_S_BLOCK_MASK = {{1, 0}, {1, 1}, {0, 1}};
    private final int[][] RIGHT_S_BLOCK_MASK = {{0, 1}, {1, 1}, {1, 0}};
    private final int[][] T_BLOCK_MASK = {{0, 1, 0}, {1, 1, 1}};

    private final int SQUARE_BLOCK_TYPE = 0;
    private final int LINE_BLOCK_TYPE = 1;
    private final int LEFT_L_BLOCK_TYPE = 2;
    private final int RIGHT_L_BLOCK_TYPE = 3;
    private final int LEFT_S_BLOCK_TYPE = 4;
    private final int RIGHT_S_TYPE = 5;
    private final int T_BLOCK_TYPE = 6;

    private int[][] blockMask;
    private int blockType;
    private Color blockColor;
    private int x, y;
    private float velocity;
    private ArrayList<Brick> bricks = new ArrayList<>();

    public Block() {
        Random random = new Random();
        int randType = random.nextInt(7);
        switch (randType) {
            case 0:
                blockType = SQUARE_BLOCK_TYPE;
                blockMask = SQUARE_BLOCK_MASK;
                break;
            case 1:
                blockType = LINE_BLOCK_TYPE;
                blockMask = LINE_BLOCK_MASK;
                break;
            case 2:
                blockType = LEFT_L_BLOCK_TYPE;
                blockMask = LEFT_L_BLOCK_MASK;
                break;
            case 3:
                blockType = RIGHT_L_BLOCK_TYPE;
                blockMask = RIGHT_L_BLOCK_MASK;
                break;
            case 4:
                blockType = LEFT_S_BLOCK_TYPE;
                blockMask = LEFT_S_BLOCK_MASK;
                break;
            case 5:
                blockType = RIGHT_S_TYPE;
                blockMask = RIGHT_S_BLOCK_MASK;
                break;
            case 6:
                blockType = T_BLOCK_TYPE;
                blockMask = T_BLOCK_MASK;
                break;
            default:
                blockType = LINE_BLOCK_TYPE;
                blockMask = LINE_BLOCK_MASK;
                break;
        }


        Color[] colors = {Color.BLUE, Color.GREEN, Color.MAGENTA, Color.RED, Color.CYAN, Color.ORANGE, Color.YELLOW};
        int pos = random.nextInt(colors.length);
        blockColor = colors[pos];

        x = (GameField.GLASS_WIDTH - blockMask[0].length) / 2;
        y = 0;

        initBricks();
    }

    private void initBricks() {
        bricks.clear();
        for (int i = 0; i < blockMask.length; i++) {
            for (int j = 0; j < blockMask[0].length; j++) {
                if (blockMask[i][j] == 1) {
                    bricks.add(new Brick(this, j, i, blockColor));
                }
            }
        }
    }

    public int[][] getBlockMask() {
        return blockMask;
    }

    public int getBlockType() {
        return blockType;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
        // setBricksOwnerPosition(x, y);
    }

    public void setY(int y) {
        this.y = y;
        //setBricksOwnerPosition(x, y);
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public ArrayList<Brick> getBricks() {
        return bricks;
    }

    public void rotate(int direction) {
        if (getBlockType() != SQUARE_BLOCK_TYPE) {
            int[][] rotateBlockMask = new int[blockMask[0].length][blockMask.length];
            for (int i = 0; i < blockMask.length; i++) {
                for (int j = 0; j < blockMask[0].length; j++) {
                    if (direction > 0) {
                        rotateBlockMask[j][rotateBlockMask[0].length - i - 1] = blockMask[i][j];
                    } else if (direction < 0) {
                        rotateBlockMask[rotateBlockMask.length - j - 1][i] = blockMask[i][j];
                    }
                }
            }
            blockMask = rotateBlockMask;
            initBricks();
        }
    }

    public void moveDown() {
        setY(getY() + 1);
    }

    public void moveSide(int direction) {
        setX(getX() + direction);
    }

    /*private void setBricksOwnerPosition(int x, int y) {
        for (Brick brick : bricks) {
            brick.setOwnerX(x);
            brick.setOwnerY(y);
        }
    }*/

    public void draw(Graphics g) {
        for (Brick brick : bricks) {
            brick.draw(g);
        }
    }

    @Override
    public int getOwnerX() {
        return x;
    }

    @Override
    public int getOwnerY() {
        return y;
    }
}
