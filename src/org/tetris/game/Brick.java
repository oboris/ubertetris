package org.tetris.game;

import org.tetris.BrickOwner;

import java.awt.*;

public class Brick {
    private int x, y/*,ownerX, ownerY*/;
    private Color color;
    private BrickOwner owner;

    /*public Brick(int ownerX, int ownerY, int x, int y, Color color) {
        this.ownerX = ownerX;
        this.ownerY = ownerY;
        this.x = x;
        this.y = y;
        this.color = color;
    }*/

    public Brick(BrickOwner owner, int x, int y, Color color) {
        this.owner = owner;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public void setOwner(BrickOwner owner) {
        this.owner = owner;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    /*public void setOwnerX(int ownerX) {
        this.ownerX = ownerX;
    }

    public void setOwnerY(int ownerY) {
        this.ownerY = ownerY;
    }*/

    public void draw(Graphics g){
        int d = GameField.CELL_SIZE;
        g.setColor(color);
        g.fillRect(GameField.MARGIN + d * (x + owner.getOwnerX()), GameField.MARGIN + d * (y + owner.getOwnerY()), GameField.CELL_SIZE, GameField.CELL_SIZE);
        g.setColor(Color.black);
        g.drawRect(GameField.MARGIN + d * (x + owner.getOwnerX()), GameField.MARGIN + d * (y + owner.getOwnerY()), GameField.CELL_SIZE, GameField.CELL_SIZE);
    }
}
